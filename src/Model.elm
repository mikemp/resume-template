module Model exposing (..)


type alias Resume =
    { name : String
    , profession : Maybe String
    , phone : Maybe String
    , email : Maybe String
    , pitch : Maybe String
    , experiences :
        List Experience
    , education :
        List Education
    }


type alias Duration =
    { from : Maybe Int
    , to : Maybe Int
    }


type Experience
    = Employment
        { employer : String
        , place : String
        , when : Duration
        , positions :
            List Position
        , pitch : Maybe String
        , contract : Bool
        }
    | Freelance
        { client : String
        , place : String
        , position : Position
        }
    | Contributor {}


type Education
    = CollegeDegree
        { degree : String
        , college : String
        , campus : Maybe String
        , year : Maybe Int
        }
    | Certification {}
    | CollegeCourse {}
    | License {}
    | Training {}


type alias Position =
    { title : String
    , when : Duration
    , pitch : Maybe String
    }
