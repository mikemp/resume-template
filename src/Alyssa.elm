module Alyssa exposing (resume)

import Model exposing (..)


resume =
    { name = "Alyssa P. Hacker"
    , profession = Just "Software Developer"
    , phone = Just "123-456-7890"
    , email = Just "lisp@hacker.com"
    , pitch = Just "The bytes are life. The bytes must flow."
    , experiences =
        [ Employment
            { employer = "Bits & Bytes Inc."
            , contract = False
            , place = "Springfield, OR"
            , when =
                { from = Just 2018
                , to = Just 2019
                }
            , positions =
                [ { title = "Intern", when = { from = Just 2018, to = Just 2019 }, pitch = Nothing }
                ]
            , pitch = Nothing
            }
        , Employment
            { employer = "Big Bites & Fries"
            , contract = False
            , place = "West Wendover, NV"
            , when =
                { from = Just 2017
                , to = Just 2018
                }
            , positions =
                [ { title = "Sales Associate", when = { from = Just 2017, to = Just 2018 }, pitch = Just "I handled the money at the register." }
                , { title = "Sales Associate II", when = { from = Just 2018, to = Just 2018 }, pitch = Just "Mad spreadsheet skillz helped pay the billz." }
                ]
            , pitch = Nothing
            }
        , Freelance
            { client = "Ma & Pa's Friendly Pet Store"
            , place = "Smalltown, USA"
            , position = { title = "Web Developer", when = { from = Just 2017, to = Just 2017 }, pitch = Just "Linux, Apache, MySQL, and PHP kept Ma & Pa in business." }
            }
        ]
    , education = []
    }
