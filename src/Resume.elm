module Resume exposing (main, view)

import Alyssa
import Basics exposing (identity)
import Browser
import Element exposing (..)
import Element.Background as EB
import Element.Font as EF
import Html exposing (Html)
import List exposing (append, map)
import Maybe exposing (map2, withDefault)
import Model exposing (..)


main =
    Browser.sandbox
        { init = Alyssa.resume
        , update = \_ model -> model
        , view = view
        }


margins =
    { top = 20
    , bottom = 20
    , right = 20
    , left = 20
    }


fontSize =
    18


nameSize =
    2 * fontSize


headerSize =
    1.25 * fontSize |> round


experienceSpacing =
    fontSize


emphasis =
    What


type Emphasis
    = Who
    | What
    | Where


style w =
    if w == emphasis then
        EF.bold

    else if w == Where then
        EF.regular

    else
        EF.italic


view : Resume -> Html msg
view resume =
    layout
        [ width fill
        , height fill
        , paddingEach margins
        ]
        (column
            [ EF.family [ EF.typeface "Courier", EF.serif ]
            , EF.size fontSize
            , height fill
            , width fill
            ]
            [ el [ centerX, EF.size nameSize ] (text resume.name)
            , el [ centerX ] (text (withDefault "" resume.profession))
            , el [ centerX ] (text (withDefault "" (Maybe.map2 (\phone email -> phone ++ " | " ++ email) resume.phone resume.email)))
            , el [ centerX, EF.italic ] (text (withDefault "" resume.pitch))
            , el [ moveDown fontSize, width fill ]
                (resume.experiences |> sortExperience |> List.reverse |> experience)
            ]
        )


experience : List Experience -> Element msg
experience es =
    case es of
        [] ->
            none

        _ ->
            column
                [ width fill, spacing fontSize ]
                (append
                    [ el
                        [ EF.size headerSize
                        , centerX
                        ]
                        (text "Professional Experience")
                    ]
                    (List.map viewExperience es)
                )


compareDuration : Duration -> Duration -> Order
compareDuration d1 d2 =
    case ( ( d1.from, d1.to ), ( d2.from, d2.to ) ) of
        ( ( Just from1, Just to1 ), ( Just from2, Just to2 ) ) ->
            if to1 == to2 then
                compare from1 from2

            else
                compare to1 to2

        ( ( Just from1, Just to1 ), ( Nothing, Just to2 ) ) ->
            if to1 == to2 then
                GT

            else
                compare to1 to2

        ( ( Nothing, Just to1 ), ( Just from2, Just to2 ) ) ->
            if to1 == to2 then
                LT

            else
                compare to1 to2

        ( ( _, Just to1 ), ( _, Nothing ) ) ->
            LT

        ( ( _, Nothing ), ( _, Just to2 ) ) ->
            GT

        _ ->
            EQ


comparePosition : Position -> Position -> Order
comparePosition p1 p2 =
    compareDuration p1.when p2.when


sortPosition : List Position -> List Position
sortPosition =
    List.sortWith comparePosition


sortExperience : List Experience -> List Experience
sortExperience =
    List.sortWith
        (\e1 e2 ->
            let
                latestPosition exp =
                    case exp of
                        Employment employment ->
                            List.head employment.positions

                        Freelance freelance ->
                            Just freelance.position

                        _ ->
                            Nothing
            in
            let
                latest1 =
                    e1 |> latestPosition

                latest2 =
                    e2 |> latestPosition
            in
            Maybe.map2 comparePosition latest1 latest2 |> Maybe.withDefault EQ
        )


viewExperience exp =
    case exp of
        Employment emp ->
            textColumn
                [ width fill ]
                ([ paragraph [ alignRight, style Where ] [ text emp.place ]
                 , paragraph [ style Who ] [ text emp.employer ]
                 ]
                    ++ List.concatMap
                        (\p ->
                            [ paragraph [ alignRight ] [ viewDuration p.when ]
                            , paragraph []
                                [ el [ style What ] (text p.title)
                                ]
                            , paragraph []
                                [ withDefault none (Maybe.map text p.pitch)
                                ]
                            ]
                        )
                        (emp.positions |> sortPosition |> List.reverse)
                )

        Freelance freelance ->
            textColumn
                [ width fill
                ]
                [ paragraph [ alignRight, style Where ] [ text freelance.place ]
                , paragraph [ style Who ] [ text freelance.client ]
                , paragraph [ alignRight ] [ viewDuration freelance.position.when ]
                , paragraph [ style What ] [ text freelance.position.title ]
                ]

        _ ->
            none


viewDuration { from, to } =
    case ( from, to ) of
        ( Just fromYear, Just toYear ) ->
            if fromYear == toYear then
                text (String.fromInt toYear)

            else
                text (String.fromInt fromYear ++ " to " ++ String.fromInt toYear)

        ( Just fromYear, Nothing ) ->
            text (String.fromInt fromYear ++ " to present")

        ( Nothing, Just toYear ) ->
            text ("Until " ++ String.fromInt toYear)

        _ ->
            none
